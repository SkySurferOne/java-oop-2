package edu.agh.javaoop;

/**
 * Created by Damian on 2016-04-13.
 */
public class ManagerA extends Manager {
    private int budget;
    private int sumOfSalaries = 0;

    public ManagerA(String name, int salary, int budget) {
        super(name, salary);
        this.budget = budget;
    }

    @Override
    public boolean hire(Employee person) {
        boolean can = super.hire(person);
        if(can) sumOfSalaries += person.getSalary();
        return can;
    }

    public boolean canHire(Employee person) {
        return ((sumOfSalaries + person.getSalary()) <= budget);
    }
}
