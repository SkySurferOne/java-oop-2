package edu.agh.javaoop;

/**
 * Created by Damian on 2016-04-13.
 */
public class Employee {
    protected String name;
    protected int salary;
    protected boolean satisfied;

    public Employee(String name, int salary) {
        this.name = name;
        this.salary = salary;
        this.satisfied = (salary > 10000) ? true : false;
    }

    public String getName() {
        return name;
    }

    public int getSalary() {
        return salary;
    }

    public boolean satisfied() {
        return satisfied;
    }
}
