package edu.agh.javaoop;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Damian on 2016-04-13.
 */
public class Company {
    private List<CEO> listOfCEOs = new ArrayList<CEO>();

    private static Company ourInstance = new Company();

    public static Company getInstance() {
        return ourInstance;
    }

    private Company() {
    }

    public void hireCEO(CEO person) {
        listOfCEOs.add(person);
    }

    @Override
    public String toString() {
        String str = "";
        for(CEO person1 : listOfCEOs){
            str += person1.getName() + " - CEO\n";
            for(Manager person2 : person1.getListOfManagers()) {
                str += "\t" + person2.getName() + " - Manager\n";
                for(Employee person3 : person2.getListOfEmployees()) {
                    str += "\t\t" + person3.getName() + " - Employee\n";
                }
            }
        }
        return str;
    }
}
