package edu.agh.javaoop;

/**
 * Created by Damian on 2016-04-13.
 */
public class ManagerB extends Manager {
    private int employeeLimit;

    public ManagerB(String name, int salary, int employeeLimit) {
        super(name, salary);
        this.employeeLimit = employeeLimit;
    }

    boolean canHire(Employee person) {
        return (listOfEmployees.size() < employeeLimit);
    }
}
