package edu.agh.javaoop;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Damian on 2016-04-13.
 */
public class CEO extends Employee {
    private List<Manager> listOfManagers = new ArrayList<Manager>();

    public CEO(String name, int salary) {
        super(name, salary);
    }

    public List<Manager> getListOfManagers() {
        return listOfManagers;
    }

    public void hireManager(Manager person) {
        listOfManagers.add(person);
    }
}
