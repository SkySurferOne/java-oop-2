package edu.agh.javaoop;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Damian on 2016-04-13.
 */
abstract class Manager extends Employee {
    protected List<Employee> listOfEmployees = new ArrayList<Employee>();

    public Manager(String name, int salary) {
        super(name, salary);
    }

    public List<Employee> getListOfEmployees() {
        return listOfEmployees;
    }

    abstract boolean canHire(Employee person);

    public boolean hire(Employee person) {
        if(canHire(person)) {
            listOfEmployees.add(person);
            return true;
        } else {
            return false;
        }
    }
}
