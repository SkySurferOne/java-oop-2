package edu.agh.javaoop;

import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;

/**
 * Created by Damian on 2016-04-13.
 */
public class ManagerTest {
    private Employee emp1, emp2, emp3, emp4, emp5;

    @Before
    public void setUp() throws Exception {
        emp1 = new Employee("Jakub", 3000);
        emp2 = new Employee("Kean", 3000);
        emp3 = new Employee("Rob", 3000);
        emp4 = new Employee("Jakub", 1000);
        emp5 = new Employee("Kean", 9000);
    }

    @Test
    public void testManagerALogic() {
        ManagerA manA = new ManagerA("Daniel", 13000, 10000);
        Assert.assertEquals(true, manA.canHire(emp1));
        manA.hire(emp1);
        manA.hire(emp2);
        manA.hire(emp3);
        Assert.assertEquals(true, manA.canHire(emp4));
        manA.hire(emp4);
        Assert.assertEquals(false, manA.canHire(emp5));
    }

    @Test
    public void testManagerBLogic() {
        ManagerB manB = new ManagerB("Zofia", 13000, 4);
        Assert.assertEquals(true, manB.canHire(emp1));
        manB.hire(emp1);
        manB.hire(emp2);
        manB.hire(emp3);
        Assert.assertEquals(true, manB.canHire(emp4));
        manB.hire(emp4);
        Assert.assertEquals(false, manB.canHire(emp5));
    }
}
