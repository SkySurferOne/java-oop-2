package edu.agh.javaoop;

import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;

/**
 * Created by Damian on 2016-04-13.
 */
public class EmployeeTest {
    private Employee emp1, emp2;

    @Before
    public void setUp() throws Exception {
        emp1 = new Employee("Jakub", 3000);
        emp2 = new Employee("Kean", 20000);
    }

    @Test
    public void testEmployeesLogic() {
        Assert.assertEquals(false, emp1.satisfied());
        Assert.assertEquals(true, emp2.satisfied());
    }
}
