package edu.agh.javaoop;

import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;

/**
 * Created by Damian on 2016-04-13.
 */
public class CompanyTest {
    private Company company = Company.getInstance();

    private void setUp() {
        // CEO
        CEO ceoInstance1 = new CEO("Marcin", 20000);
        company.hireCEO(ceoInstance1);
        // Manager
        Manager manA = new ManagerA("Daniel", 13000, 100000);
        Manager manB = new ManagerB("Maria", 16000, 200000);
        ceoInstance1.hireManager(manA);
        ceoInstance1.hireManager(manB);
        //  Employee
        Employee emp1 = new Employee("Jakub", 7000);
        Employee emp2 = new Employee("Kean", 9000);
        Employee emp3 = new Employee("Rob", 10000);
        manA.hire(emp1);
        manB.hire(emp2);
        manB.hire(emp3);
    }

    @Test
    public void testCompanyToString() {
        setUp();

        String str = "Marcin - CEO\n" +
                    "\tDaniel - Manager\n" +
                    "\t\tJakub - Employee\n" +
                    "\tMaria - Manager\n" +
                    "\t\tKean - Employee\n" +
                    "\t\tRob - Employee\n";
        System.out.println(company.toString());
        Assert.assertEquals(str, company.toString());

    }
}
